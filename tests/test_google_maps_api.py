from utils.api_google import GoogleMaps
from utils.checking import Checking
import allure

@allure.epic("Test create palce")
class TestCreatePlace:

    @allure.description("Test create,update, delete new palce")
    def test_create_new_place(self):
        """This method tests the creation, update, and deletion of a new place using Google Maps API"""


        print("POST method")
        result_post = GoogleMaps.create_new_place()  # Perform a POST request to create a new place
        check_post = result_post.json()  # Extract the JSON response from the result
        place_id = check_post.get('place_id')  # Retrieve the place_id from the JSON response
        Checking.check_status_code(result_post, 200)  # Check the status code of the POST request
        Checking.check_json_token(result_post, ['status', 'place_id', 'scope', 'reference', 'id'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_value(result_post, 'status', 'OK')  # Check the value of the 'status' key in the JSON response


        print("GET method (checking POST)")
        result_get = GoogleMaps.get_new_place(place_id)  # Perform a GET request to retrieve the newly created place
        Checking.check_status_code(result_get, 200)  # Check the status code of the GET request
        Checking.check_json_token(result_get, ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website', 'language'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_value(result_get, 'address', '29, side layout, cohen 09')  # Check the value of the 'address' key in the JSON response


        print("PUT method")
        result_put = GoogleMaps.put_new_place(place_id)  # Perform a PUT request to update the created place
        Checking.check_status_code(result_put, 200)  # Check the status code of the PUT request
        Checking.check_json_token(result_put, ['msg'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_value(result_put, 'msg', 'Address successfully updated')  # Check the value of the 'msg' key in the JSON response


        print("GET method (checking PUT)")
        result_get = GoogleMaps.get_new_place(place_id)  # Perform a GET request to retrieve the updated place
        Checking.check_status_code(result_get, 200)  # Check the status code of the GET request
        Checking.check_json_token(result_get, ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website', 'language'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_value(result_get, 'address', '100 Lenina street, RU')  # Check the value of the 'address' key in the JSON response


        print("DELETE method")
        result_delete = GoogleMaps.delete_new_place(place_id)  # Perform a DELETE request to delete the created place
        Checking.check_status_code(result_delete, 200)  # Check the status code of the DELETE request
        Checking.check_json_token(result_delete, ['status'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_value(result_delete, 'status', 'OK')  # Check the value of the 'status' key in the JSON response


        print("GET method (checking DELETE)")
        result_get = GoogleMaps.get_new_place(place_id)  # Perform a GET request to check if the place was successfully deleted
        Checking.check_status_code(result_get, 404)  # Check the status code of the GET request
        Checking.check_json_token(result_get, ['msg'])  # Check if the required JSON tokens are present in the response
        Checking.check_json_word_value(result_get, 'msg', 'looks')  # Check if the word 'looks' is present in the value of the 'msg' key in the JSON response

        print("Testing the creation, update, and deletion of the new location passed successfully!!!")
