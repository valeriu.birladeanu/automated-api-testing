"""Methods for checking the responses to requests"""
import json
from requests import Response


class Checking:
    """This class provides methods for checking the responses to requests."""

    @staticmethod
    def check_status_code(response: Response, status_code: int) -> None:
        """
        Check if the response status code matches the expected status code.
        Args:
            response (Response): The response object obtained from the request.
            status_code (int): The expected status code.
        Raises:
            AssertionError: If the response status code does not match the expected status code.
        """
        assert status_code == response.status_code
        if response.status_code == status_code:
            print("Correct status code: " + str(response.status_code))
        else:
            print("Fail status code: " + str(response.status_code))

    @staticmethod
    def check_json_token(response: Response, expected_value: list) -> None:
        """
        Check if the required fields are present in the response JSON.
        Args:
            response (Response): The response object obtained from the request.
            expected_value (list): The list of expected field names.
        Raises:
            AssertionError: If the required fields are not present in the response JSON.
        """
        token = json.loads(response.text)
        assert list(token) == expected_value
        print("The field is present")

    @staticmethod
    def check_json_value(response: Response, field_name: str, expected_value: str) -> None:
        """
        Check if the required field in the response JSON has the expected value.
        Args:
            response (Response): The response object obtained from the request.
            field_name (str): The name of the field to check.
            expected_value (str): The expected value of the field.
        Raises:
            AssertionError: If the field does not have the expected value.
        """
        check = response.json()
        check_info = check.get(field_name)
        assert check_info == expected_value
        print('Correct value: ' + expected_value)

    @staticmethod
    def check_json_word_value(response: Response, field_name: str, word: str) -> None:
        """
        Check if the required field in the response JSON contains the given word.
        Args:
            response (Response): The response object obtained from the request.
            field_name (str): The name of the field to check.
            word (str): The word to search for in the field.
        Raises:
            AssertionError: If the word is not present in the field.
        """
        check = response.json()
        check_info = check.get(field_name)
        if word in check_info:
            print('Is present Word: ' + word )
        else:
            print('Is not present Word: ' + word )
