import requests
from utils.logger import Logger
import allure


"""List of HTTP methods"""
class HttpMethods:
    headers = {'Content-Type': 'application/json'}  # All headers will be sent in JSON format
    cookie = ""

    """Creating custom methods based on the requests library"""
    @staticmethod
    def get(url):
        """Sends an HTTP GET request to the specified URL"""
        with allure.step("GET"):
            Logger.add_request(url, method="GET")
            result = requests.get(url, headers=HttpMethods.headers, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def post(url, body):
        """Sends an HTTP POST request to the specified URL with the provided body data"""
        with allure.step("POST"):
            Logger.add_request(url, method="POST")
            result = requests.post(url, json=body, headers=HttpMethods.headers, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def put(url, body):
        """Sends an HTTP PUT request to the specified URL with the provided body data"""
        with allure.step("PUT"):
            Logger.add_request(url, method="PUT")
            result = requests.put(url, json=body, headers=HttpMethods.headers, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result

    @staticmethod
    def delete(url, body):
        """Sends an HTTP DELETE request to the specified URL with the provided body data"""
        with allure.step("DELETE"):
            Logger.add_request(url, method="DELETE")
            result = requests.delete(url, json=body, headers=HttpMethods.headers, cookies=HttpMethods.cookie)
            Logger.add_response(result)
            return result
