from utils.http_methods import HttpMethods

"""Methods for testing the Google Maps API"""

base_url = 'https://rahulshettyacademy.com'
key = '?key=qaclick123'

class GoogleMaps:
    """The method for creating a new location"""
    @staticmethod
    def create_new_place():
        # JSON payload for creating a new place
        json_for_create_new_place = {
            "location": {
                "lat": -38.383494,
                "lng": 33.427362
            },
            "accuracy": 50,
            "name": "Frontline house",
            "phone_number": "(+91) 983 893 3937",
            "address": "29, side layout, cohen 09",
            "types": [
                "shoe park",
                "shop"
            ],
            "website": "http://google.com",
            "language": "French-IN"
        }

        post_resource = '/maps/api/place/add/json'  # Resource method POST
        post_url = base_url + post_resource + key
        print(post_url)
        result_post = HttpMethods.post(post_url, json_for_create_new_place)
        print(result_post.text)
        return result_post

    """Method for checking the presence of a location"""
    @staticmethod
    def get_new_place(place_id):
        get_resource = '/maps/api/place/get/json'  # Resource method GET
        get_url = base_url + get_resource + key + '&place_id=' + place_id
        print(get_url)
        result_get = HttpMethods.get(get_url)
        print(result_get.text)
        return result_get

    """Method for changing a location"""
    @staticmethod
    def put_new_place(place_id):
        # JSON payload for updating a location
        json_for_update_new_place = {
            "place_id": place_id,
            "address": "100 Lenina street, RU",
            "key": "qaclick123"
        }
        put_resource = '/maps/api/place/update/json'  # Resource method PUT
        put_url = base_url + put_resource + key
        print(put_url)
        result_put = HttpMethods.put(put_url, json_for_update_new_place)
        print(result_put.text)
        return result_put

    """Method for deleting a location"""
    @staticmethod
    def delete_new_place(place_id):
        # JSON payload for deleting a location
        json_for_delete_new_place = {
            "place_id": place_id
        }
        delete_resource = '/maps/api/place/delete/json'  # Resource method DELETE
        delete_url = base_url + delete_resource + key
        print(delete_url)
        result_delete = HttpMethods.put(delete_url, json_for_delete_new_place)
        print(result_delete.text)
        return result_delete
